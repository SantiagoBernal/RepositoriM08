# M08Curs1920

## Curs 2019-2020 de M08-Aplicacions Web

# UF1 - Ofimàtica i eines Web

## P1: Correu electrònic i Calendari Web

*[Enunciat](/uf1/p1.md)*

### Solucions:

* **[Jofre](https://gitlab.com/46980410n/m08/blob/master/UF%201/Pr%C3%A0ctica%201.md)**
* **[Dani](https://gitlab.com/45878162q/m08/blob/master/UF1/p1.md)**
* **[Santi](https://gitlab.com/ar843234/modulo-08/blob/master/UF1/P01/P01_uf1_p1.md)**
* **[Uíliam](https://gitlab.com/54987812x/m08-um/blob/master/UF1/p1.md)**

## P2: Eines ofimàtiques Web

*[Enunciat](/uf1/p2.md)* 

### Solucions:

* **[Jofre](https://gitlab.com/46980410n/m08/blob/master/UF%201/Pr%C3%A0ctica%2002.md)**
* **[Dani](https://gitlab.com/45878162q/m08/blob/master/UF1/p2.md)**
* **[Uíliam](https://gitlab.com/54987812x/m08-um/blob/master/UF1/p2.md)**

## P3: Eines de gestió de grups i aplicacions socials

*[Enunciat](/uf1/p3.md)* 

### Solucions:

* **[Jofre](https://gitlab.com/46980410n/m08/blob/master/UF%201/Pr%C3%A0ctica%2003.md)**
* **[Manu](https://gitlab.com/36579859s/m08/blob/master/UF1/P3.md)**
* **[Uíliam](https://gitlab.com/54987812x/m08-um/blob/master/UF1/p3.md)**
* **[Santi](https://gitlab.com/ar843234/modulo-08/blob/master/UF1/P03/P3.md)**

## P4: Sistemes operatius web

*[Enunciat](/uf1/p4.md)* 

### Solucions:

* **[Jofre](https://gitlab.com/46980410n/m08/edit/master/UF%201/Pr%C3%A0ctica%2004.md)**
* **[Uíliam](https://gitlab.com/54987812x/m08-um/blob/master/UF1/p4.md)**
* **[Santi](https://gitlab.com/ar843234/modulo-08/blob/master/UF1/p04/P04.md)**


# PRESENTACIONS:

[Guia per fer la presentació](/uf1/presentacio.md)

* Uiliam - ASANA.
* Jofre - GMAIL.
* Santi - SLIDESHARE.
* Manu - DOODLE.
* Jordi - BUFFER.
* Dani - PREZI.
* Lennon - TRELLO.

## [Programa en Python per sortejar els temes](/python/sorteig.py) 


## **Curriculums dels alumnes:**

* **[Lennon](https://gitlab.com/15159795/m08/blob/master/curriculum.md)**
* **[Jordi](https://gitlab.com/46986035w/curriculum-jordi/blob/master/curriculum_markdown.md)**
* **[Achraf](https://gitlab.com/y5302499r/m08/blob/master/curriculum.md)**
* **[Santiago](https://gitlab.com/ar843234/modulo-08/blob/master/Curriculum.md)**
* **[Uiliam](https://gitlab.com/54987812x/m08-um/blob/master/curriculum.md)**
* **[Daniel](https://gitlab.com/45878162q/m08/blob/master/curriculum.md)**
* **[Jofre](https://gitlab.com/46980410n/m08/blob/master/curriculum.md)**
* **[Manu](https://gitlab.com/36579859s/m08/blob/master/UF1/Curriculum.md)**

**Exemple de curriculum:**
[cv](https://gitlab.com/xsancho/m08curs1920/blob/master/curriculum.md)