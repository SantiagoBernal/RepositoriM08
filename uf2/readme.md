# UF2 - Gestió d'arxius Web:

* [**Pràctica 1** - Instal·lació i configuració d' Alfresco a Linux.](p1.md)
* [**Pràctica 2** - Instal·lació i configuració de Owncloud a Linux.](p2.md)
* [**Pràctica 3** - Instal·lació i configuració de git a Linux.](p3.md)